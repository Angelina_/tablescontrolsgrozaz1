﻿namespace TableEvents
{
    public class PropShowDialog
    {
        private static bool isShowDialogOpen = false;
        public static bool IsShowDialogOpen
        {
            get { return isShowDialogOpen; }
            set
            {
                if (isShowDialogOpen == value)
                    return;

                isShowDialogOpen = value;
            }
        }
    }

    public class PropNumPointsDFs
    {
        //private static int selectedNumPointsDFs = 2;// для отладки
        private static int selectedNumPointsDFs = 0;
        public static int SelectedNumPointsDFs
        {
            get { return selectedNumPointsDFs; }
            set
            {
                if (selectedNumPointsDFs == value)
                    return;

                selectedNumPointsDFs = value;
            }
        }
    }

    public class PropIsRecPointsDFs
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }

    public class PropViewCoords
    { 
        private static byte viewCoords;
        public static byte ViewCoords
        {
            get { return viewCoords; }
            set
            {
                if (viewCoords == value)
                    return;

                viewCoords = value;
            }
        }
    }

    public class PropNumUAV
    {
        private static int selectedIdOwnUAV = 0;
        public static int SelectedIdOwnUAV
        {
            get { return selectedIdOwnUAV; }
            set
            {
                if (selectedIdOwnUAV == value)
                    return;

                selectedIdOwnUAV = value;
            }
        }

        private static int selectedNumOwnUAV = 0;
        public static int SelectedNumOwnUAV
        {
            get { return selectedNumOwnUAV; }
            set
            {
                if (selectedNumOwnUAV == value)
                    return;

                selectedNumOwnUAV = value;
            }
        }

        private static int selectedNumEscope = 0;
        public static int SelectedNumEscope
        {
            get { return selectedNumEscope; }
            set
            {
                if (selectedNumEscope == value)
                    return;

                selectedNumEscope = value;
            }
        }

        private static string selectedSerialNumEscope = string.Empty;
        public static string SelectedSerialNumEscope
        {
            get { return selectedSerialNumEscope; }
            set
            {
                if (selectedSerialNumEscope == value)
                    return;

                selectedSerialNumEscope = value;
            }
        }

        private static bool isSelectedNumEscope = false;
        public static bool IsSelectedNumEscope
        {
            get { return isSelectedNumEscope; }
            set
            {
                if (isSelectedNumEscope == value)
                    return;

                isSelectedNumEscope = value;
            }
        }
    }

    public class PropFrequencies
    {
        private static int selectedIndFrequencies = -1;
        public static int SelectedIndFrequencies
        {
            get { return selectedIndFrequencies; }
            set
            {
                if (selectedIndFrequencies == value)
                    return;

                selectedIndFrequencies = value;
            }
        }
    }

}
