﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableEvents
{
    public class SelectedRowEvents
    {
        public int Id { get; }

        public SelectedRowEvents(int id)
        {
            Id = id;
        }
    }
}
