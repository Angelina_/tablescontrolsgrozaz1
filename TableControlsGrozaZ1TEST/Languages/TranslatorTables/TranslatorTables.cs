﻿using System;
using System.ComponentModel;
using System.Windows;

namespace TableControlsGrozaZ1TEST
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void UcOwnUAV_OnIsWindowPropertyOpen(object sender, OwnUAVControl.OwnUAVProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }

        private void UcPointsDFs_OnIsWindowPropertyOpen(object sender, PointsDFsControl.PointsDFsProperty e)
        {
            e.SetLanguagePropertyGrid(basicProperties.Local.General.Language);
        }
    }
}
