﻿using System;
using System.ComponentModel;
using System.Windows;
using ValuesCorrectLib;

namespace TableControlsGrozaZ1TEST
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private void SetLanguageTables(DllGrozaZProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaZProperties.Models.Languages.EN:
                        dict.Source = new Uri("/Groza-Z AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaZProperties.Models.Languages.RU:
                        dict.Source = new Uri("/Groza-Z AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                                            UriKind.Relative);
                        break;

                    default:
                        dict.Source = new Uri("/Groza-Z AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllGrozaZProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/TableControlsGrozaZ1TEST;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.EN.xaml",
                //                      UriKind.Relative);
                //        break;
                //    case DllGrozaZProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/TableControlsGrozaZ1TEST;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                //                           UriKind.Relative);
                //        break;
                //    default:
                //        dict.Source = new Uri("/TableControlsGrozaZ1TEST;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST


                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        private void basicProperties_OnLanguageChanged(object sender, DllGrozaZProperties.Models.Languages language)
        {
            SetLanguageTables(language);
            TranslatorTables.LoadDictionary(language);
        }

       
    }
}
