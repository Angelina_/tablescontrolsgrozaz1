﻿using GrpcDbClientLib;
using InheritorsEventArgs;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace TableControlsGrozaZ1TEST
{
    public partial class MainWindow
    {
        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void HandlerUpData(object sender, DataEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                //TextBoxMessage.Foreground = Brushes.White;
                //TextBoxMessage.AppendText($"{DateTime.Now}\nLoad data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            });
        }

        private void HandlerDisconnect(object sender, ClientEventArgs eventArgs)
        {
            if (eventArgs.GetMessage != "")
            {
                DbControlConnection.ShowDisconnect();
            }
            clientDB = null;
        }

        private void HandlerConnect(object sender, ClientEventArgs e)
        {
            DbControlConnection.ShowConnect();
            LoadTables();
        }

        private void DbConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.Disconnect();
                else
                {
                    clientDB = new ServiceClient(this.Name, endPoint, serverPort);
                    InitClientDB();
                    clientDB.Connect();
                }
            }
            catch (ExceptionClient exceptClient)
            {
                HandlerDisconnect(this, null);
                MessageBox.Show(exceptClient.Message);
            }
        }
    }
}
