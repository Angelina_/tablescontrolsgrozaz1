﻿using InheritorsEventArgs;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using TableEvents;
using GrpcDbClientLib;
using System.Collections.Generic;
using GrozaZ1ModelsDBLib;

namespace TableControlsGrozaZ1TEST
{
    public partial class MainWindow
    {
        private void OnUpTable_TableOwnUAV(object sender, TableEventArgs<TableOwnUAV> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lOwnUAV = e.Table;
                ucOwnUAV.UpdateOwnUAV(lOwnUAV);
            });
        }

        private void OnUpTable_TablePoint(object sender, TableEventArgs<TablePoint> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                lPoints = e.Table;
                ucPointsDFs.UpdatePointsDFs(lPoints);
            });
        }

        private async void LoadTables()
        {
            try
            {
                lOwnUAV = await clientDB.Tables[NameTable.TableOwnUAV].LoadAsync<TableOwnUAV>();
                ucOwnUAV.UpdateOwnUAV(lOwnUAV);

                lPoints = await clientDB.Tables[NameTable.TablePoint].LoadAsync<TablePoint>();
                ucPointsDFs.UpdatePointsDFs(lPoints);

            }
            catch (ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (GrpcDbClientLib.ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
