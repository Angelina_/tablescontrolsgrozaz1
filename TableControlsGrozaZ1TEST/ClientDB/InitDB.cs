﻿using System;
using GrpcDbClientLib;
using InheritorsEventArgs;
using GrozaZ1ModelsDBLib;

namespace TableControlsGrozaZ1TEST
{
    public partial class MainWindow
    {
        ServiceClient clientDB;
      
        string endPoint = "127.0.0.1";
        int serverPort = 30051;

        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;

            (clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += OnUpTable_TableOwnUAV;
            (clientDB.Tables[NameTable.TablePoint] as ITableUpdate<TablePoint>).OnUpTable += OnUpTable_TablePoint;
        }

       
    }
}
