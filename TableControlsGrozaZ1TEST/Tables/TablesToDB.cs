﻿using GrozaZ1ModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableEvents;

namespace TableControlsGrozaZ1TEST
{
    public partial class MainWindow
    {
        // Свои БПЛА 
        public List<TableOwnUAV> lOwnUAV = new List<TableOwnUAV>();
        // Пункты ОП
        public List<TablePoint> lPoints = new List<TablePoint>();


        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Add(e.Record);
            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (clientDB != null)
            {
                clientDB.Tables[nameTable].Clear();
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (clientDB != null)
            {
                clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }
    }
}
