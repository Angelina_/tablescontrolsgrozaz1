﻿using DllGrozaZProperties.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;
using ValuesCorrectLib;

namespace TableControlsGrozaZ1TEST
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";
            // DbConnection_Click(this, null);

            //InitTables();
            PropViewCoords.ViewCoords = (byte)1;
            SetLanguageTables(Languages.EN);
            TranslatorTables.LoadDictionary(Languages.EN);
            ucOwnUAV.UpdatePoints(new List<int>() { 11111, 112, 113 });
            //ucPointsDFs.IsRsVisible = false;
            ucPointsDFs.HasRs = false;
            //SetLanguageTables(basicProperties.Local.General.Language);
            //TranslatorTables.LoadDictionary(basicProperties.Local.General.Language);

        }

    }
}
