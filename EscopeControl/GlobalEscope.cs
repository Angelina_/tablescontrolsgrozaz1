﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace EscopeControl
{
    public class GlobalEscope
    {
        public ObservableCollection<EscopeUAVModel> CollectionEscope { get; set; }
        public EscopeUAVModel SelectedItem { get; set; }

        public GlobalEscope()
        {
            try
            {
                CollectionEscope = new ObservableCollection<EscopeUAVModel>();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
