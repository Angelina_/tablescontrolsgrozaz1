﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace EscopeControl
{
    /// <summary>
    /// Interaction logic for EscopeControl.xaml
    /// </summary>
    public partial class EscopeControl : UserControl
    {
        #region Events
        public event EventHandler<string> OnDeleteRecord;
        public event EventHandler OnClearRecords;
        public event EventHandler OnSaveTracks;
        public event EventHandler<EscopeUAVModel> OnCentering;
        public event EventHandler<EscopeUAVModel> OnSendToJamming;
        public event EventHandler<EscopeUAVModel> OnAddOwn;
        #endregion

        private GlobalEscope tableViewModel = new GlobalEscope();

        public EscopeControl()
        {
            InitializeComponent();

            this.DataContext = tableViewModel;
        }


        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (tableViewModel.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    //TableAeroscope tableAeroscope = new TableAeroscope
                    //{
                    var serial = tableViewModel.SelectedItem.SerialNumber;
                    //};

                    // Событие удаления одной записи
                    //OnDeleteRecord(this, new TableEvent(tableAeroscope));
                    OnDeleteRecord?.Invoke(this, serial);
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords?.Invoke(this, null);
            }
            catch { }
        }

        private void DgvAeroscope_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvAeroscope_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tableViewModel.SelectedItem == null) return;

            if (tableViewModel.SelectedItem.SerialNumber != string.Empty)
            {
                if (tableViewModel.SelectedItem.SerialNumber != PropNumUAV.SelectedSerialNumEscope)
                {
                    int ind = tableViewModel.CollectionEscope.ToList().FindIndex(x => x.SerialNumber == tableViewModel.SelectedItem.SerialNumber);
                    if (ind != -1)
                    {
                        PropNumUAV.SelectedNumEscope = ind;
                        PropNumUAV.SelectedSerialNumEscope = tableViewModel.SelectedItem.SerialNumber;
                        PropNumUAV.IsSelectedNumEscope = true;
                    }
                }
            }
            else
            {
                PropNumUAV.SelectedNumEscope = 0;
                PropNumUAV.SelectedSerialNumEscope = string.Empty;
                PropNumUAV.IsSelectedNumEscope = false;
            }
        }

        private void DgvAeroscope_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (tableViewModel.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие Центрирование карты по выбранному источнику
                    OnCentering?.Invoke(this, tableViewModel.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }

        private void ButtonSaveTracks_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnSaveTracks?.Invoke(sender, null);
            }
            catch { }
        }

        private void SendForJammingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (tableViewModel.SelectedItem == null) 
                    return;
                
                if (!IsSelectedRowEmpty())
                    return;

                var record = tableViewModel.SelectedItem;

                OnSendToJamming?.Invoke(this, record);
            }
            catch { }
        }

        private void AddAsOwnMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnAddOwn?.Invoke(sender, tableViewModel.SelectedItem);
            }
            catch { }
        }
    }
}
