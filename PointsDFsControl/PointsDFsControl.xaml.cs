﻿using GrozaZ1ModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableEvents;

namespace PointsDFsControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlPointsDFs : UserControl
    {
        public PointsDFsProperty PointsDFsWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };

        public event EventHandler OnClickButtonZ1;

        public event EventHandler<SelectedRowEvents> OnSelectedRow = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<SelectedRowEvents> OnDoubleClickPointsDFs = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<SelectedRowEvents> OnGetCoords = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<SelectedRowEvents> OnGetTime = (object sender, SelectedRowEvents data) => { };

        // Открылось окно с PropertyGrid
        public event EventHandler<PointsDFsProperty> OnIsWindowPropertyOpen = (object sender, PointsDFsProperty data) => { };
        #endregion

        public UserControlPointsDFs()
        {
            InitializeComponent();

            DgvPointsDFs.DataContext = new GlobalPointsDFs();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PointsDFsWindow = new PointsDFsProperty(((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs);

                this.IsBrowsablePropertyHasRs(this.HasRs, this.PointsDFsWindow);

                OnIsWindowPropertyOpen(this, PointsDFsWindow);

                if (!PropIsRecPointsDFs.IsRecAdd && !PropIsRecPointsDFs.IsRecChange)
                {
                    PointsDFsWindow.Show();
                    PropIsRecPointsDFs.IsRecAdd = true;
                    PointsDFsWindow.OnAddRecordPG += new EventHandler<MyTablePointsDFs>(PointsDFsWindow_OnAddRecordPG);
                }
            }
            catch { }
        }

        private void PointsDFsWindow_OnAddRecordPG(object sender, MyTablePointsDFs e)
        {
            PropIsRecPointsDFs.IsRecAdd = false;

            // Событие добавления одной записи
            OnAddRecord(this, new TableEvent(MyTablePointsDFsToTablePoint(e)));

        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((MyTablePointsDFs)DgvPointsDFs.SelectedItem != null)
                {
                    if (((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id > 0)
                    {
                        var selected = (MyTablePointsDFs)DgvPointsDFs.SelectedItem;

                        PointsDFsWindow = new PointsDFsProperty(((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs, selected.Clone());
                        this.IsBrowsablePropertyHasRs(this.HasRs, this.PointsDFsWindow);
                        OnIsWindowPropertyOpen(this, PointsDFsWindow);

                        if (!PropIsRecPointsDFs.IsRecAdd && !PropIsRecPointsDFs.IsRecChange)
                        {
                            PointsDFsWindow.Show();
                            PropIsRecPointsDFs.IsRecChange = true;
                            PointsDFsWindow.OnChangeRecordPG += new EventHandler<MyTablePointsDFs>(PointsDFsWindow_OnChangeRecordPG);
                        }
                    }
                }
            }
            catch { }
        }

        private void PointsDFsWindow_OnChangeRecordPG(object sender, MyTablePointsDFs e)
        {
            PropIsRecPointsDFs.IsRecChange = false;

            // Событие изменения одной записи
            OnChangeRecord(this, new TableEvent(MyTablePointsDFsToTablePoint(e)));
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((MyTablePointsDFs)DgvPointsDFs.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    TablePoint tablePoint = new TablePoint
                    {
                        Id = ((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id
                    };

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent(tablePoint));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTable.TablePoint);
            }
            catch { }
        }

        private void DgvPointsDFs_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvPointsDFs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((MyTablePointsDFs)DgvPointsDFs.SelectedItem == null) return;

            if (((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id > -1)
            {
                if (((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id != PropNumPointsDFs.SelectedNumPointsDFs)
                {
                    PropNumPointsDFs.SelectedNumPointsDFs = ((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id;

                    OnSelectedRow(this, new SelectedRowEvents(PropNumPointsDFs.SelectedNumPointsDFs));
                }
            }
            else
            {
                PropNumPointsDFs.SelectedNumPointsDFs = 0;
            }
        }

        private void DgvPointsDFs_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((MyTablePointsDFs)DgvPointsDFs.SelectedItem == null) return;

            if (((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id > -1)
            {
                OnDoubleClickPointsDFs(this, new SelectedRowEvents(((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id));
            }
        }

        private void ButtonZ1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnClickButtonZ1(this, null);
            }
            catch { }
           
        }

        private bool hasRs = true;
        public bool HasRs
        {
            get => hasRs;
            set
            {
                if (hasRs == value)
                    return;

                hasRs = value;
                ColumnVisible(hasRs);
            }
        }


        private void ColumnVisible(bool isRsVisible)
        {
            try
            {
                if (isRsVisible)
                {
                    dgtIpRs.Visibility = Visibility.Visible;
                    dgtPortRs.Visibility = Visibility.Visible;
                }
                else
                {
                    dgtIpRs.Visibility = Visibility.Hidden;
                    dgtPortRs.Visibility = Visibility.Hidden;
                }
                
                
            }
            catch { }
        }

        private void IsBrowsablePropertyHasRs(bool hasRs, PointsDFsProperty ASPWindow)
        {
            if (hasRs)
            {
                ASPWindow.propertyGrid.Properties[nameof(MyTablePointsDFs.PortRDS)].IsBrowsable = true;
                ASPWindow.propertyGrid.Properties[nameof(MyTablePointsDFs.IpAddrRDS)].IsBrowsable = true;
            }
            else
            {
                ASPWindow.propertyGrid.Properties[nameof(MyTablePointsDFs.PortRDS)].IsBrowsable = false;
                ASPWindow.propertyGrid.Properties[nameof(MyTablePointsDFs.IpAddrRDS)].IsBrowsable = false;
            }
        }
    }
}
