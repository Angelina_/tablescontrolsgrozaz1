﻿using GrozaZ1ModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TableEvents;

namespace PointsDFsControl
{
    public partial class UserControlPointsDFs : UserControl
    {
        public List<MyTablePointsDFs> TempListPointsDFs { get; set; }

        /// <summary>
        /// Обновить Пункты
        /// </summary>
        /// <param name="listPoints"></param>
        public void UpdatePointsDFs(List<TablePoint> listPoints)
        {
            try
            {
                if (listPoints == null)
                    return;

                ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Clear();

                List<MyTablePointsDFs> list = TablePointToMyTablePointsDFs(listPoints);

                TempListPointsDFs = list;

                for (int i = 0; i < listPoints.Count; i++)
                {
                    ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Add(list[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.ToList().FindIndex(x => x.Id == PropNumPointsDFs.SelectedNumPointsDFs);
                if (ind != -1)
                {
                    DgvPointsDFs.SelectedIndex = ind;
                }
                else
                {
                    DgvPointsDFs.SelectedIndex = 0;
                }

            }
            catch { }
        }

        /// <summary>
        ///  Преобразование List<TablePoint> к List<MyTablePointsDFs>
        /// </summary>
        /// <param name="listPointsDFs"></param>
        /// <returns> List<MyTablePointsDFs> </returns>
        private List<MyTablePointsDFs> TablePointToMyTablePointsDFs(List<TablePoint> listPointsDFs)
        {
            List<MyTablePointsDFs> list = new List<MyTablePointsDFs>();
            for (int i = 0; i < listPointsDFs.Count; i++)
            {
                MyTablePointsDFs table = new MyTablePointsDFs();

                double minutesLat = (listPointsDFs[i].Coordinates.Latitude - Math.Truncate(listPointsDFs[i].Coordinates.Latitude)) * 60;
                double minutesLon = (listPointsDFs[i].Coordinates.Longitude - Math.Truncate(listPointsDFs[i].Coordinates.Longitude)) * 60;

                table.Id = listPointsDFs[i].Id;
                table.Coordinates.Altitude = listPointsDFs[i].Coordinates.Altitude;
                table.Coordinates.Latitude = listPointsDFs[i].Coordinates.Latitude;
                table.Coordinates.Longitude = listPointsDFs[i].Coordinates.Longitude;
                table.IpAddrTDF = listPointsDFs[i].IpAddrTDF;
                table.PortTDF = listPointsDFs[i].PortTDF;
                table.IpAddrRDS = listPointsDFs[i].IpAddrRDS;
                table.PortRDS = listPointsDFs[i].PortRDS;
                table.Note = listPointsDFs[i].Note;
                table.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(listPointsDFs[i].Coordinates.Latitude);
                table.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(listPointsDFs[i].Coordinates.Longitude);
                table.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((listPointsDFs[i].Coordinates.Latitude - Math.Truncate(listPointsDFs[i].Coordinates.Latitude)) * 60, 4);
                table.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((listPointsDFs[i].Coordinates.Longitude - Math.Truncate(listPointsDFs[i].Coordinates.Longitude)) * 60, 4);
                table.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((listPointsDFs[i].Coordinates.Latitude - Math.Truncate(listPointsDFs[i].Coordinates.Latitude)) * 60);
                table.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((listPointsDFs[i].Coordinates.Longitude - Math.Truncate(listPointsDFs[i].Coordinates.Longitude)) * 60);
                table.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                table.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);
                table.CoordinatesDDMMSS.Altitude = listPointsDFs[i].Coordinates.Altitude;
                table.CourseAngle = listPointsDFs[i].CourseAngle;
                table.IsGnssUsed = listPointsDFs[i].IsGnssUsed;

                list.Add(table);
            }

            return list;
        }

        /// <summary>
        /// Преобразование MyTablePointsDFs к TablePoint
        /// </summary>
        /// <param name="tablePointsDFs"></param>
        /// <returns></returns>
        private TablePoint MyTablePointsDFsToTablePoint(MyTablePointsDFs tablePointsDFs)
        {
            TablePoint table = new TablePoint();

            table.Id = tablePointsDFs.Id;
            table.IpAddrTDF = tablePointsDFs.IpAddrTDF;
            table.PortTDF = tablePointsDFs.PortTDF;
            table.IpAddrRDS = tablePointsDFs.IpAddrRDS;
            table.PortRDS = tablePointsDFs.PortRDS;
            table.Note = tablePointsDFs.Note;
            table.CourseAngle = tablePointsDFs.CourseAngle;
            table.IsGnssUsed = tablePointsDFs.IsGnssUsed;

            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    table.Coordinates.Altitude = tablePointsDFs.Coordinates.Altitude;
                    table.Coordinates.Latitude = tablePointsDFs.Coordinates.Latitude;
                    table.Coordinates.Longitude = tablePointsDFs.Coordinates.Longitude;
                    break;

                case 2: // format "DD MM.mmmm"
                    table.Coordinates.Altitude = tablePointsDFs.CoordinatesDDMMSS.Altitude;
                    table.Coordinates.Latitude = Math.Round(tablePointsDFs.CoordinatesDDMMSS.LatDegrees + ((double)tablePointsDFs.CoordinatesDDMMSS.LatMinutesDDMM / 60), 6);
                    table.Coordinates.Longitude = Math.Round(tablePointsDFs.CoordinatesDDMMSS.LonDegrees + ((double)tablePointsDFs.CoordinatesDDMMSS.LonMinutesDDMM / 60), 6);
                    break;

                case 3: // format "DD MM SS.ss"

                    table.Coordinates.Altitude = tablePointsDFs.Coordinates.Altitude;
                    table.Coordinates.Latitude = Math.Round(tablePointsDFs.CoordinatesDDMMSS.LatDegrees + ((double)tablePointsDFs.CoordinatesDDMMSS.LatMinutesDDMMSS / 60) + (tablePointsDFs.CoordinatesDDMMSS.LatSeconds / 3600), 6);
                    table.Coordinates.Longitude = Math.Round(tablePointsDFs.CoordinatesDDMMSS.LonDegrees + ((double)tablePointsDFs.CoordinatesDDMMSS.LonMinutesDDMMSS / 60) + (tablePointsDFs.CoordinatesDDMMSS.LonSeconds / 3600), 6);
                    break;
                default:
                    break;
            }
            
            return table;
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((MyTablePointsDFs)DgvPointsDFs.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

        /// <summary>
        /// Добавить пустые строки в таблицу
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvPointsDFs.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvPointsDFs.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvPointsDFs.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.RemoveAt(index);
                    }
                }

                //List<MyTablePointsDFs> list = new List<MyTablePointsDFs>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    MyTablePointsDFs strP = new MyTablePointsDFs
                    {
                        Id = -2,
                        Coordinates = new Coord() { Altitude = -2, Latitude = -2, Longitude = -2 },
                        IpAddrTDF = string.Empty,
                        PortTDF = -2,
                        IpAddrRDS = string.Empty,
                        PortRDS = -2,
                        Note = string.Empty,
                        CourseAngle = -2
                    };
                    ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Add(strP);
                }

                //for (int i = 0; i < list.Count; i++)
                //{
                //    ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Add(list[i]);
                //}
            }
            catch { }
        }

        /// <summary>
        /// Удалить пустые строки из таблицы
        /// </summary>
        private void DeleteEmptyRows()
        {
            try
            {
                int countEmptyRows = ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Count(s => s.Id < 0);
                int countAllRows = ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Count;
                int iCount = countAllRows - countEmptyRows;
                for (int i = iCount; i < countAllRows; i++)
                {
                    ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.RemoveAt(iCount);
                }
            }
            catch { }
        }

        /// <summary>
        /// Установить Пункт ОП
        /// </summary>
        /// <param name="coord"></param>
        public void SetPointsDFsToPG(Coord coord)
        {
            try
            {
                if (PointsDFsWindow != null)
                {
                    switch (PropViewCoords.ViewCoords)
                    {
                        case 1: // format "DD.dddddd"

                            PointsDFsWindow.PointsDFs.Coordinates.Latitude = coord.Latitude;
                            PointsDFsWindow.PointsDFs.Coordinates.Longitude = coord.Longitude;

                            break;

                        case 2: // format "DD MM.mmmm"

                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(coord.Latitude);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(coord.Longitude);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LatMinutesDDMM = Math.Round((coord.Latitude - Math.Truncate(coord.Latitude)) * 60, 4);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LonMinutesDDMM = Math.Round((coord.Longitude - Math.Truncate(coord.Longitude)) * 60, 4);
                           
                            break;

                        case 3: // format "DD MM SS.ss"
                            double minutesLat = (coord.Latitude - Math.Truncate(coord.Latitude)) * 60;
                            double minutesLon = (coord.Longitude - Math.Truncate(coord.Longitude)) * 60;

                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LatDegrees = (int)Math.Truncate(coord.Latitude);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LonDegrees = (int)Math.Truncate(coord.Longitude);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LatMinutesDDMMSS = (int)((coord.Latitude - Math.Truncate(coord.Latitude)) * 60);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LonMinutesDDMMSS = (int)((coord.Longitude - Math.Truncate(coord.Longitude)) * 60);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LatSeconds = Math.Round((minutesLat - Math.Truncate(minutesLat)) * 60, 2);
                            PointsDFsWindow.PointsDFs.CoordinatesDDMMSS.LonSeconds = Math.Round((minutesLon - Math.Truncate(minutesLon)) * 60, 2);

                            break;
                        default:
                            break;
                    }
                }
            }
            catch { }
        }
    }
}
