﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace PointsDFsControl
{
    public partial class UserControlPointsDFs : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion



        #region FormatViewCoord

        public static readonly DependencyProperty ViewCoordProperty = DependencyProperty.Register("ViewCoord", typeof(byte), typeof(UserControlPointsDFs),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(ViewCoordChanged)));

        public byte ViewCoord
        {
            get { return (byte)GetValue(ViewCoordProperty); }
            set { SetValue(ViewCoordProperty, value); }
        }

        private static void ViewCoordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlPointsDFs userControlPointsDFs = (UserControlPointsDFs)d;
                userControlPointsDFs.UpdateFormatCoords();
            }
            catch
            { }

        }
        #endregion

        /// <summary>
        /// Обновить вид координат в таблице
        /// </summary>
        private void UpdateFormatCoords()
        {
            try
            {
                if (TempListPointsDFs == null) return;

                PropViewCoords.ViewCoords = ViewCoord;

                ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Clear();

                for (int i = 0; i < TempListPointsDFs.Count; i++)
                {
                    ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.Add(TempListPointsDFs[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalPointsDFs)DgvPointsDFs.DataContext).CollectionPointsDFs.ToList().FindIndex(x => x.Id == PropNumPointsDFs.SelectedNumPointsDFs);
                if (ind != -1)
                {
                    DgvPointsDFs.SelectedIndex = ind;
                }
                else
                {
                    DgvPointsDFs.SelectedIndex = 0;
                }
            }
            catch { }
        }

       
    }
}
