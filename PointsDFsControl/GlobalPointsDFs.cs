﻿using GrozaZ1ModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace PointsDFsControl
{
    public class GlobalPointsDFs
    {
        public ObservableCollection<MyTablePointsDFs> CollectionPointsDFs { get; set; }

        public GlobalPointsDFs()
        {
            try
            {
                CollectionPointsDFs = new ObservableCollection<MyTablePointsDFs> { };

                //CollectionPointsDFs = new ObservableCollection<MyTablePointsDFs>
                //{
                //    new MyTablePointsDFs
                //    {
                //        Id = 1,
                //        IpAddrRDS = "127.0.0.1",
                //        IpAddrTDF = "127.0.0.1",
                //        PortRDS = 1234,
                //        PortTDF = 2009,
                //        Note = "MJJL :LJL",
                //        Coordinates = new Coord{Latitude = 57.535535, Longitude = 24.4454543, Altitude = 500.6F}
                //    }
                //};
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }

    [DataContract]
    [CategoryOrder("Общие", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    public class MyTablePointsDFs
    {
        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Id)), ReadOnly(false)]
        [Browsable(true)]
        public int Id { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Прочее")]
        [PropertyOrder(2)]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [PropertyOrder(1)]
        [DisplayName(nameof(CourseAngle))]
        public int CourseAngle { get; set; } = -1;

        [DataMember]
        [Category("Общие")]
        [PropertyOrder(1)]
        [DisplayName(nameof(IpAddrTDF))]
        public string IpAddrTDF { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [PropertyOrder(2)]
        [DisplayName(nameof(PortTDF))]
        public int PortTDF { get; set; }

        [DataMember]
        [Category("Общие")]
        [PropertyOrder(3)]
        [DisplayName(nameof(IpAddrRDS))]
        public string IpAddrRDS { get; set; } = string.Empty;

        [DataMember]
        [Category("Общие")]
        [PropertyOrder(4)]
        [DisplayName(nameof(PortRDS))]
        public int PortRDS { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CoordDDMMSS CoordinatesDDMMSS { get; set; } = new CoordDDMMSS();

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(nameof(IsGnssUsed))]
        [PropertyOrder(2)]
        public bool IsGnssUsed { get; set; }

        public MyTablePointsDFs Clone()
        {
            return new MyTablePointsDFs
            {
                Id = Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                IpAddrTDF = IpAddrTDF,
                PortTDF = PortTDF,
                IpAddrRDS = IpAddrRDS,
                PortRDS = PortRDS,
                Note = Note,
                CoordinatesDDMMSS = new CoordDDMMSS
                {
                    LatDegrees = CoordinatesDDMMSS.LatDegrees,
                    LonDegrees = CoordinatesDDMMSS.LonDegrees,
                    LatMinutesDDMM = CoordinatesDDMMSS.LatMinutesDDMM,
                    LonMinutesDDMM = CoordinatesDDMMSS.LonMinutesDDMM,
                    LatMinutesDDMMSS = CoordinatesDDMMSS.LatMinutesDDMMSS,
                    LonMinutesDDMMSS = CoordinatesDDMMSS.LonMinutesDDMMSS,
                    LatSeconds = CoordinatesDDMMSS.LatSeconds,
                    LonSeconds = CoordinatesDDMMSS.LonSeconds,
                    Altitude = CoordinatesDDMMSS.Altitude
                },
                CourseAngle = CourseAngle,
                IsGnssUsed = IsGnssUsed
            };
        }
    }

    public class CoordDDMMSS : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion


        private int latDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LatDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatDegrees
        {
            get { return latDegrees; }
            set
            {
                if (latDegrees == value)
                    return;

                latDegrees = value;
                OnPropertyChanged(nameof(LatDegrees));
            }
        }

        private int lonDegrees = 0;
        [DataMember]
        [DisplayName(nameof(LonDegrees))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonDegrees
        {
            get { return lonDegrees; }
            set
            {
                if (lonDegrees == value)
                    return;

                lonDegrees = value;
                OnPropertyChanged(nameof(LonDegrees));
            }
        }

        private double latMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatMinutesDDMM
        {
            get { return latMinutesDDMM; }
            set
            {
                if (latMinutesDDMM == value)
                    return;

                latMinutesDDMM = value;
                OnPropertyChanged(nameof(LatMinutesDDMM));
            }
        }

        private double lonMinutesDDMM = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMM))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonMinutesDDMM
        {
            get { return lonMinutesDDMM; }
            set
            {
                if (lonMinutesDDMM == value)
                    return;

                lonMinutesDDMM = value;
                OnPropertyChanged(nameof(LonMinutesDDMM));
            }
        }

        private int latMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LatMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LatMinutesDDMMSS
        {
            get { return latMinutesDDMMSS; }
            set
            {
                if (latMinutesDDMMSS == value)
                    return;

                latMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LatMinutesDDMMSS));
            }
        }

        private int lonMinutesDDMMSS = 0;
        [DataMember]
        [DisplayName(nameof(LonMinutesDDMMSS))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public int LonMinutesDDMMSS
        {
            get { return lonMinutesDDMMSS; }
            set
            {
                if (lonMinutesDDMMSS == value)
                    return;

                lonMinutesDDMMSS = value;
                OnPropertyChanged(nameof(LonMinutesDDMMSS));
            }
        }

        private double latSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LatSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LatSeconds
        {
            get { return latSeconds; }
            set
            {
                if (latSeconds == value)
                    return;

                latSeconds = value;
                OnPropertyChanged(nameof(LatSeconds));
            }
        }

        private double lonSeconds = 0;
        [DataMember]
        [DisplayName(nameof(LonSeconds))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double LonSeconds
        {
            get { return lonSeconds; }
            set
            {
                if (lonSeconds == value)
                    return;

                lonSeconds = value;
                OnPropertyChanged(nameof(LonSeconds));
            }
        }

        private float altitude = -1;
        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;

                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }
    }



}
