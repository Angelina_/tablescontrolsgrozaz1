﻿using DllGrozaZProperties.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using TableEvents;
using ValuesCorrectLib;

namespace PointsDFsControl
{
    /// <summary>
    /// Логика взаимодействия для PointsDFsProperty.xaml
    /// </summary>
    public partial class PointsDFsProperty : Window
    {
        public event EventHandler<MyTablePointsDFs> OnAddRecordPG = (object sender, MyTablePointsDFs data) => { };
        public event EventHandler<MyTablePointsDFs> OnChangeRecordPG = (object sender, MyTablePointsDFs data) => { };

        private ObservableCollection<MyTablePointsDFs> collectionTemp;
        public MyTablePointsDFs PointsDFs { get; private set; }

        public PointsDFsProperty(ObservableCollection<MyTablePointsDFs> collectionPointsDFs)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionPointsDFs;
                PointsDFs = new MyTablePointsDFs();

                propertyGrid.SelectedObject = PointsDFs;
                propertyGrid.Properties[nameof(MyTablePointsDFs.Id)].IsReadOnly = false;

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"
                        propertyGrid.Properties[nameof(MyTablePointsDFs.CoordinatesDDMMSS)].IsBrowsable = false;
                        break;

                    case 2: // format "DD MM.mmmm"
                    case 3: // format "DD MM SS.ss"
                        propertyGrid.Properties[nameof(MyTablePointsDFs.Coordinates)].IsBrowsable = false;
                        break;

                    default:
                        break;
                }

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;

                //ChangeCategories();
            }
            catch { }
        }

        public PointsDFsProperty(ObservableCollection<MyTablePointsDFs> collectionPointsDFs, MyTablePointsDFs tablePointsDFs)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionPointsDFs;
                PointsDFs = tablePointsDFs;
                if (PointsDFs.Coordinates.Latitude != -1)
                {
                    PointsDFs.Coordinates.Latitude = PointsDFs.Coordinates.Latitude < 0 ? PointsDFs.Coordinates.Latitude * -1 : PointsDFs.Coordinates.Latitude;
                    PointsDFs.Coordinates.Latitude = Math.Round(PointsDFs.Coordinates.Latitude, 6);
                }
                if (PointsDFs.Coordinates.Longitude != -1)
                {
                    PointsDFs.Coordinates.Longitude = PointsDFs.Coordinates.Longitude < 0 ? PointsDFs.Coordinates.Longitude * -1 : PointsDFs.Coordinates.Longitude;
                    PointsDFs.Coordinates.Longitude = Math.Round(PointsDFs.Coordinates.Longitude, 6);
                }
                propertyGrid.SelectedObject = PointsDFs;

                propertyGrid.Properties[nameof(MyTablePointsDFs.Id)].IsReadOnly = true;

                switch (PropViewCoords.ViewCoords)
                {
                    case 1: // format "DD.dddddd"
                        propertyGrid.Properties[nameof(MyTablePointsDFs.CoordinatesDDMMSS)].IsBrowsable = false;
                        break;

                    case 2: // format "DD MM.mmmm"
                    case 3: // format "DD MM SS.ss"
                        propertyGrid.Properties[nameof(MyTablePointsDFs.Coordinates)].IsBrowsable = false;
                        break;

                    default:
                        break;
                }

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                //ChangeCategories();
                InitProperty();
                propertyGrid.Categories.FirstOrDefault(t => t.Name == "Coordinates").HeaderCategoryName = SMeaning.meaningCoord;

            }
            catch { }
        }

        public PointsDFsProperty()
        {
            InitializeComponent();

            InitEditors();
            //ChangeCategories();
            InitProperty();
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((MyTablePointsDFs)propertyGrid.SelectedObject) != null)
            {
                if (PropIsRecPointsDFs.IsRecAdd)
                {
                    OnAddRecordPG?.Invoke(sender, (MyTablePointsDFs)propertyGrid.SelectedObject);
                }

                if (PropIsRecPointsDFs.IsRecChange)
                {
                    OnChangeRecordPG?.Invoke(sender, (MyTablePointsDFs)propertyGrid.SelectedObject);
                }

                Close();
                //DialogResult = true;
            }
        }


        public MyTablePointsDFs IsAddClick(MyTablePointsDFs PointsDFsWindow)
        {
            //CorrectParams.IsCorrectMinMax(JammerStationWindow);
            //if (CorrectParams.IsCorrectFreqMinMax(JammerStationWindow.FreqMinKHz, FreqRanges.FreqMaxKHz))
            //{
            //    JammerStationWindow.IsActive = true;
            //    return JammerStationWindow;
            //}
            return PointsDFsWindow;
            //return null;
        }

        private void ButtonNoApply_Click(object sender, RoutedEventArgs e)
        {
            Close();
            PropIsRecPointsDFs.IsRecAdd = false;
            PropIsRecPointsDFs.IsRecChange = false;

            //DialogResult = false;
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void InitEditors()
        {
            switch (PropViewCoords.ViewCoords)
            {
                case 1: // format "DD.dddddd"
                    propertyGrid.Editors.Add(new CoordinatesDDPointsDFsEditor(nameof(PointsDFs.Coordinates), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new IpAddrTDFDDPointsDFsEditor(nameof(PointsDFs.IpAddrTDF), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new IpAddrRDSDDPointsDFsEditor(nameof(PointsDFs.IpAddrRDS), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new NoteDDPointsDFsEditor(nameof(PointsDFs.Note), typeof(MyTablePointsDFs)));
                    break;

                case 2: // format "DD MM.mmmm"
                    propertyGrid.Editors.Add(new CoordinatesDDMMPointsDFsEditor(nameof(PointsDFs.CoordinatesDDMMSS), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new IpAddrTDFDDMMPointsDFsEditor(nameof(PointsDFs.IpAddrTDF), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new IpAddrRDSDDMMPointsDFsEditor(nameof(PointsDFs.IpAddrRDS), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new NoteDDMMPointsDFsEditor(nameof(PointsDFs.Note), typeof(MyTablePointsDFs)));
                    break;

                case 3: // format "DD MM SS.ss"
                    propertyGrid.Editors.Add(new CoordinatesDDMMSSPointsDFsEditor(nameof(PointsDFs.CoordinatesDDMMSS), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new IpAddrTDFDDMMSSPointsDFsEditor(nameof(PointsDFs.IpAddrTDF), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new IpAddrRDSDDMMSSPointsDFsEditor(nameof(PointsDFs.IpAddrRDS), typeof(MyTablePointsDFs)));
                    propertyGrid.Editors.Add(new NoteDDMMSSPointsDFsEditor(nameof(PointsDFs.Note), typeof(MyTablePointsDFs)));
                    break;

                default:
                    break;
            }
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((MyTablePointsDFs)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        public void SetLanguagePropertyGrid(DllGrozaZProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            TranslatorTables.ChangeLanguagePropertyGrid(language, propertyGrid);
            //LoadTranslatorPropertyGrid(Languages.EN);
            //TranslatorTables.ChangeLanguagePropertyGrid(Languages.EN, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaZProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaZProperties.Models.Languages.EN:
                        dict.Source = new Uri("/Groza-Z AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.EN.xaml",
                                      UriKind.Relative);
                        break;

                    case DllGrozaZProperties.Models.Languages.RU:
                        dict.Source = new Uri("/Groza-Z AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                                            UriKind.Relative);
                        break;

                    default:
                        dict.Source = new Uri("/Groza-Z AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                // TEST ------------------------------------------------------------------------------------------------------
                //switch (language)
                //{
                //    case DllGrozaZProperties.Models.Languages.EN:
                //        dict.Source = new Uri("/TableControlsGrozaZ1TEST;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.EN.xaml",
                //                      UriKind.Relative);
                //        break;

                //    case DllGrozaZProperties.Models.Languages.RU:
                //        dict.Source = new Uri("/TableControlsGrozaZ1TEST;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                //                            UriKind.Relative);
                //        break;

                //    default:
                //        dict.Source = new Uri("/TableControlsGrozaZ1TEST;component/Languages/TranslatorTables/TranslatorTablesGrozaZ1.RU.xaml",
                //                          UriKind.Relative);
                //        break;
                //}
                // ------------------------------------------------------------------------------------------------------ TEST

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropIsRecPointsDFs.IsRecAdd = false;
            PropIsRecPointsDFs.IsRecChange = false;
        }
    }
}
