﻿using GrozaZ1ModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TableEvents;

namespace OwnUAVControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlOwnUAV : UserControl
    {
        public OwnUAVProperty OwnUAVWindow;

        #region Events
        public event EventHandler<TableEvent> OnAddRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnChangeRecord = (object sender, TableEvent data) => { };
        public event EventHandler<TableEvent> OnDeleteRecord = (object sender, TableEvent data) => { };
        public event EventHandler<NameTable> OnClearRecords = (object sender, NameTable data) => { };
        // public event EventHandler<SelectedRowEvents> OnSelectedRow = (object sender, SelectedRowEvents data) => { };
        public event EventHandler<(TableOwnUAV, int)> OnSendRecord = (object sender, (TableOwnUAV, int) data) => { };
        public event EventHandler<TableOwnUAV> OnSignalRec = (object sender, TableOwnUAV data) => { };
        // Открылось окно с PropertyGrid
        public event EventHandler<OwnUAVProperty> OnIsWindowPropertyOpen = (object sender, OwnUAVProperty data) => { };

        #endregion

        #region Properties
        public NameTable NameTableOwnUAV { get; } = NameTable.TableOwnUAV;
        public NameTable NameTableOwnUAVFreq { get; } = NameTable.TableOwnUAVFreq;

        public ObservableCollection<int> Points
        {
            get { return (ObservableCollection<int>)GetValue(AntennaProperty); }
            set { SetValue(AntennaProperty, value); }
        }

        public static readonly DependencyProperty AntennaProperty =
            DependencyProperty.Register("Points", typeof(ObservableCollection<int>),
            typeof(UserControlOwnUAV), new FrameworkPropertyMetadata(new ObservableCollection<int>()));
        #endregion

        public UserControlOwnUAV()
        {
            InitializeComponent();

            DgvOwnUAV.DataContext = new GlobalOwnUAV();
            DgvOwnUAVFreq.DataContext = new GlobalOwnUAVFreq();
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OwnUAVWindow = new OwnUAVProperty(((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV);

                OnIsWindowPropertyOpen(this, OwnUAVWindow);

                if (OwnUAVWindow.ShowDialog() == true)
                {
                    // Событие добавления одной записи
                    OnAddRecord(this, new TableEvent(OwnUAVWindow.OwnUAV));
                }
            }
            catch { }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableOwnUAV)DgvOwnUAV.SelectedItem != null)
                {
                    if (((TableOwnUAV)DgvOwnUAV.SelectedItem).Id > 0)
                    {
                        var selected = (TableOwnUAV)DgvOwnUAV.SelectedItem;

                        OwnUAVWindow = new OwnUAVProperty(((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV, selected.Clone());

                        OnIsWindowPropertyOpen(this, OwnUAVWindow);

                        if (OwnUAVWindow.ShowDialog() == true)
                        {
                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvent(OwnUAVWindow.OwnUAV));
                        }
                    }
                }
            }
            catch { }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableOwnUAV)DgvOwnUAV.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvent((TableOwnUAV)DgvOwnUAV.SelectedItem));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, NameTableOwnUAV);
            }
            catch { }
        }

        private void DgvOwnUAV_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvOwnUAVFreq_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRowsFreq();
        }

        private void DgvOwnUAV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((TableOwnUAV)DgvOwnUAV.SelectedItem == null) return;

            if (((TableOwnUAV)DgvOwnUAV.SelectedItem).Id > -1)
            {
                if (((TableOwnUAV)DgvOwnUAV.SelectedItem).Id != PropNumUAV.SelectedIdOwnUAV)
                {
                    int ind = ((GlobalOwnUAV)DgvOwnUAV.DataContext).CollectionOwnUAV.ToList().FindIndex(x => x.Id == ((TableOwnUAV)DgvOwnUAV.SelectedItem).Id);
                    if (ind != -1)
                    {
                        PropNumUAV.SelectedNumOwnUAV = ind;
                        PropNumUAV.SelectedIdOwnUAV = ((TableOwnUAV)DgvOwnUAV.SelectedItem).Id;

                        UpdateOwnUAVFreq((((TableOwnUAV)DgvOwnUAV.SelectedItem).Frequencies).ToList());

                        // OnSelectedRow(this, new SelectedRowEvents(PropNumUAV.SelectedIdOwnUAV));
                    }
                }
            }
            else
            {
                PropNumUAV.SelectedNumOwnUAV = 0;
                PropNumUAV.SelectedIdOwnUAV = 0;
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Id > 0)
                {
                    if (((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IsActive)
                    {
                        ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvent((TableOwnUAV)DgvOwnUAV.SelectedItem));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableOwnUAV)DgvOwnUAV.SelectedItem == null) return;

                if (((TableOwnUAV)DgvOwnUAV.SelectedItem).Id > -1)
                {
                    if ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem == null) return;

                    if (((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Id > -1)
                    {
                        TableOwnUAV table = new TableOwnUAV
                        {
                            Id = ((TableOwnUAV)DgvOwnUAV.SelectedItem).Id,
                            SerialNumber = ((TableOwnUAV)DgvOwnUAV.SelectedItem).SerialNumber,
                            Frequencies = new ObservableCollection<TableOwnUAVFreq>
                            {
                                new TableOwnUAVFreq
                                {
                                    Id = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Id,
                                    IdSR = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IdSR,
                                    BandMHz = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).BandMHz,
                                    FrequencyMHz = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).FrequencyMHz,
                                    IsActive = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IsActive,
                                    SR = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).SR,
                                    TableOwnUAVId = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).TableOwnUAVId,
                                    Time = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Time
                                }
                            }
                        };

                        OnSendRecord(this, (table, (int)cmbPoints.SelectedValue));
                    }
                }

            }
            catch { }

        }

        private void ButtonSignalRec_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((TableOwnUAV)DgvOwnUAV.SelectedItem == null) return;

                if (((TableOwnUAV)DgvOwnUAV.SelectedItem).Id > -1)
                {
                    if ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem == null) return;

                    if (((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Id > -1)
                    {
                        TableOwnUAV table = new TableOwnUAV
                        {
                            Id = ((TableOwnUAV)DgvOwnUAV.SelectedItem).Id,
                            SerialNumber = ((TableOwnUAV)DgvOwnUAV.SelectedItem).SerialNumber,
                            Frequencies = new ObservableCollection<TableOwnUAVFreq>
                            {
                                new TableOwnUAVFreq
                                {
                                    Id = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Id,
                                    IdSR = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IdSR,
                                    BandMHz = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).BandMHz,
                                    FrequencyMHz = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).FrequencyMHz,
                                    IsActive = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).IsActive,
                                    SR = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).SR,
                                    TableOwnUAVId = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).TableOwnUAVId,
                                    Time = ((TableOwnUAVFreq)DgvOwnUAVFreq.SelectedItem).Time
                                }
                            }
                        };

                        OnSignalRec(this, table);
                    }
                }

            }
            catch { }

        }

        public void UpdatePoints(List<int> list)
        {
            Points.Clear();
            foreach(var item in list) Points.Add(item);
        }

        private bool hasRs = true;
        public bool HasRs
        {
            get => hasRs;
            set
            {
                if (hasRs == value)
                    return;

                hasRs = value;
                ColumnVisible(hasRs);
            }
        }


        private void ColumnVisible(bool isRsVisible)
        {
            try
            {
                if (isRsVisible)
                {
                    spRsRec.Visibility = Visibility.Visible;
                    dgtRsHasSignal.Visibility = Visibility.Visible;
                    dgtIsActive.Visibility = Visibility.Visible;
                }
                else
                {
                    spRsRec.Visibility = Visibility.Hidden;
                    dgtRsHasSignal.Visibility = Visibility.Hidden;
                    dgtIsActive.Visibility = Visibility.Hidden;
                }


            }
            catch { }
        }
    }
}

