﻿using System.Windows.Forms;

namespace ValuesCorrectLib
{
    public class CorrectParams
    {
        /// <summary>
        /// Проверка на корректность ввода значений частоты
        /// </summary>
        /// <param name="iFreqMin"> начальное значение частоты </param>
        /// <param name="iFreqMax"> конечное значение частоты </param>
        /// <returns> true - успешно, false - нет </returns>
        //private static bool IsCorrectFreqMinMax(int iFreqMin, int iFreqMax)
        public static bool IsCorrectFreqMinMax(double dFreqMin, double dFreqMax)
        {
            bool bCorrect = true;

            if (dFreqMin >= dFreqMax)
            {
                MessageBox.Show(SMessages.mesValuesMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }


        /// <summary>
        /// Проверка на корректность ввода значений сектора
        /// </summary>
        /// <param name="iAngleMin"> начальное значение сектора </param>
        /// <param name="iAngleMax"> конечное значение сектора </param>
        /// <returns> true - успешно, false - нет </returns>
        public static bool IsCorrectAngleMinMax(short iAngleMin, short iAngleMax)
        {
            bool bCorrect = true;

            if (iAngleMin >= iAngleMax)
            {
                MessageBox.Show(SMessages.mesValuesAngleMaxMin, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }

        public static bool IsCorrectSerialNumber(string serialNumber)
        {
            return serialNumber.Length < 14 ? false : true;
        }

        public static bool IsCorrectFreq(double dFreq)
        {
            bool bCorrect = true;

            if (dFreq < 100 || dFreq > 6000)
            {
                MessageBox.Show(SMessages.mesFreqBelongBand, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bCorrect = false;
            }

            return bCorrect;
        }


    }
}
