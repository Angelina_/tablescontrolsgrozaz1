﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ValuesCorrectLib
{
    #region Задание координат в градусах (DD)
    public class CoordinatesDDPointsDFsEditor : PropertyEditor
    {
        public CoordinatesDDPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDPointsDFsEditorKey"];
        }
    }
    public class IpAddrTDFDDPointsDFsEditor : PropertyEditor
    {
        public IpAddrTDFDDPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["IpAddrTDFDDPointsDFsEditorKey"];
        }
    }

    public class IpAddrRDSDDPointsDFsEditor : PropertyEditor
    {
        public IpAddrRDSDDPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["IpAddrRDSDDPointsDFsEditorKey"];
        }
    }

    public class NoteDDPointsDFsEditor : PropertyEditor
    {
        public NoteDDPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDD.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["NoteDDPointsDFsEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах минутах (DDMM)
    public class CoordinatesDDMMPointsDFsEditor : PropertyEditor
    {
        public CoordinatesDDMMPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMPointsDFsEditorKey"];
        }
    }

    public class IpAddrTDFDDMMPointsDFsEditor : PropertyEditor
    {
        public IpAddrTDFDDMMPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["IpAddrTDFDDMMPointsDFsEditorKey"];
        }
    }

    public class IpAddrRDSDDMMPointsDFsEditor : PropertyEditor
    {
        public IpAddrRDSDDMMPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["IpAddrRDSDDMMPointsDFsEditorKey"];
        }
    }

    public class NoteDDMMPointsDFsEditor : PropertyEditor
    {
        public NoteDDMMPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMM.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["NoteDDMMPointsDFsEditorKey"];
        }
    }
    #endregion

    #region Задание координат в градусах минутах секундах (DDMMSS)
    public class CoordinatesDDMMSSPointsDFsEditor : PropertyEditor
    {
        public CoordinatesDDMMSSPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["CoordinatesDDMMSSPointsDFsEditorKey"];
        }
    }

    public class IpAddrTDFDDMMSSPointsDFsEditor : PropertyEditor
    {
        public IpAddrTDFDDMMSSPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["IpAddrTDFDDMMSSPointsDFsEditorKey"];
        }
    }

    public class IpAddrRDSDDMMSSPointsDFsEditor : PropertyEditor
    {
        public IpAddrRDSDDMMSSPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["IpAddrRDSDDMMSSPointsDFsEditorKey"];
        }
    }

    public class NoteDDMMSSPointsDFsEditor : PropertyEditor
    {
        public NoteDDMMSSPointsDFsEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/PointsDFsControl;component/Themes/PropertyGridEditorPointsDFsDDMMSS.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["NoteDDMMSSPointsDFsEditorKey"];
        }
    }
    #endregion


}
